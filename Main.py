import urllib, json
from operator import itemgetter
url = "https://gist.githubusercontent.com/lastperson/b1b434eaa8c09fa4560fc8ac8eceafdc/raw/b8fe263a4a3ae98f92c2f1471c6482b3f941a38b/oxotestdata.json";
response = urllib.urlopen(url);
data = json.loads(response.read());
p = (data[0][0]);

our_win_combinations_all_x = (([0, 0, u'X'], [1, 1, u'X'], [2, 2, u'X']),
                               ([0, 2, u'X'], [1, 1, u'X'], [2, 0, u'X']),
                               ([0, 0, u'X'], [1, 0, u'X'], [2, 0, u'X']),
                               ([0, 1, u'X'], [1, 1, u'X'], [2, 1, u'X']),
                               ([0, 2, u'X'], [1, 2, u'X'], [2, 2, u'X']),
                               ([0, 0, u'X'], [0, 1, u'X'], [0, 2, u'X']), ([1, 0, u'X'], [1, 1, u'X'], [1, 2, u'X']),
                               ([2, 0, u'X'], [2, 1, u'X'], [2, 2, u'X']));

our_win_combinations_all_o = (([0, 0, u'O'], [1, 1, u'O'], [2, 2, u'O']), ([0, 2, u'O'], [1, 1, u'O'], [2, 0, u'O']),
                               ([0, 0, u'O'], [1, 0, u'O'], [2, 0, u'O']), ([0, 1, u'O'], [1, 1, u'O'], [2, 1, u'O']),
                               ([0, 2, u'O'], [1, 2, u'O'], [2, 2, u'O']), ([0, 0, u'O'], [0, 1, u'O'], [0, 2, u'O']),
                               ([1, 0, u'O'], [1, 1, u'O'], [1, 2, u'O']), ([2, 0, u'O'], [2, 1, u'O'], [2, 2, u'O']));
_sortedList = [];
for i in data:
    if len(i) == 9:
        sortedList = sorted(i, key=itemgetter(1, 0));
        _sortedList.append(sortedList);

def drawGame(i):
    row_count = 0;
    result_string = "";
    for a in i:
        result_string += a[2] + " ";
        row_count += 1;
        if row_count == 3:
            result_string = result_string + "\n"
            row_count = 0;
    print (result_string)


_victoriesX = 0;
_victoriesO = 0;
_countAll = 0;
_steps = 0;
_draws = 0;

# test item for the list - to test if algorithm works in case of addition of an one more victory X, victory O or draw
#  _sortedList.append([[0, 0, u'O'], [1, 0, u'X'], [2, 0, u'O'], [0, 1, u'X'], [1, 1, u'O'], [2, 1, u'X'], [0, 2, u'O'], [1, 2, u'O'], [2, 2, u'X']]);

for i in _sortedList:
    # drawGame(i);
    _countAll += 1;
    for a, b in zip(our_win_combinations_all_x, our_win_combinations_all_o):
        a = list(a);
        b = list(b);
        if all(x in i for x in a):
            # drawGame(i);
            _victoriesX += 1;

        elif all(x in i for x in b):
            # drawGame(i);
            _victoriesO += 1;
        else:
            # drawGame(i);
            _steps += 1;

_draws = _countAll-_victoriesX-_victoriesO;

print "All games with 9 steps= " + str(_countAll);
print "Count X victories= " + str(_victoriesX);
print "Count O victories= " + str(_victoriesO);
print "Draws count= " + str(_draws);
print "Steps = " + str(_steps + _victoriesO + _victoriesX);